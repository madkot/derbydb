package myjdbc;
import java.sql.*;
import java.awt.*; import javax.swing.*; import javax.swing.table.*;
public class JTableResultSet extends JFrame {
    private JTable jTable;
    public JTableResultSet(String arg0) throws Exception {
        super(arg0);
        ResultSetTableModel model = getModel();
        jTable = new JTable(model);
        TableRowSorter<TableModel> sorter =
                new TableRowSorter<TableModel>(model);
        jTable.setRowSorter(sorter);
        JScrollPane jScrollPane = new JScrollPane(jTable);
        getContentPane().add(jScrollPane, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500, 200);
        getModel();
        setVisible(true);
    }

    private ResultSetTableModel getModel() throws SQLException {
        Statement statement =
                DriverManager.getConnection("jdbc:derby:myjdbc;create=true").
                        createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                ResultSet.CONCUR_READ_ONLY);
        ResultSet resultSet =
                statement.executeQuery(
                        "select * from piosenki join artysci on artysci.id = artysci.id");
        return new ResultSetTableModel(resultSet);
    }
    public static void main(String[] args) throws Exception {
        new JTableResultSet("Show data in JTable");
    }
}