package myjdbc;

import java.sql.*;
public class ResultSetExamples {
    public static void main(String[] args) throws SQLException {
        Statement statement = DriverManager.getConnection("jdbc:derby:myjdbc;create=true").
                createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
        ResultSet resultSet = statement.executeQuery("select * from users");
        if(resultSet.first()){
            System.out.println("First row -> \t\t"
                    + ResultSetExamples.getDataFromRow(resultSet));
        }
        if(resultSet.last()){
            System.out.println("Last row -> \t\t"
                    + ResultSetExamples.getDataFromRow(resultSet));
        }
        if(resultSet.relative(-2)){
            System.out.println("relative row -2 -> \t"
                    + ResultSetExamples.getDataFromRow(resultSet));
        }
        if(resultSet.absolute(-2)){
            System.out.println("absolute row -2 -> \t"
                    + ResultSetExamples.getDataFromRow(resultSet));
        }
    }
    public static String getDataFromRow(ResultSet _resultSet) throws SQLException{
        String userName = _resultSet.getString("username");
        String password = _resultSet.getString("password");
        String firstName = _resultSet.getString(3);
        String lastName = _resultSet.getString(4);
        return new String("userName=" + userName + ", password=" + password
                + ", firstName=" + firstName +", lastName="
                + lastName + ", row=" + _resultSet.getRow());
    }
}