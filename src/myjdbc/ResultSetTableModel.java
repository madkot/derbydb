package myjdbc;

import java.sql.*;
import javax.swing.table.AbstractTableModel;

public class ResultSetTableModel extends AbstractTableModel {
    private ResultSet resultSet;
    private ResultSetMetaData rsmd;
    public ResultSetTableModel(ResultSet aResultSet) {
        resultSet = aResultSet;
        try {
            rsmd = resultSet.getMetaData();
        } catch (SQLException e) {
            System.out.println("Error " + e);
        }
    }
    @Override
    public String getColumnName(int c) {
        try {
            return rsmd.getColumnName(c + 1);
        } catch (SQLException e) {
            System.out.println("Error " + e); return "";
        }
    }


    @Override
    public int getColumnCount() {
        try {
            return rsmd.getColumnCount();
        } catch (SQLException e) { e.printStackTrace(); return 0;}
    }
    @Override
    public int getRowCount() {
        try {
            resultSet.last();
            return resultSet.getRow();
        } catch (SQLException e) { return 0; }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            resultSet.absolute(rowIndex + 1);
            return resultSet.getObject(columnIndex + 1);
        } catch (SQLException e) {e.printStackTrace(); return null;}
    }
}