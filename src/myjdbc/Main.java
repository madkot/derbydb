package myjdbc;

import java.sql.*;
import java.util.Scanner;


public class Main {
    Connection konn;
    public static void main(String[] args) {
        Conn baza = new Conn();
        int zadanie;
        //baza.addUsers();
        //baza.dropTable();
        //baza.createTable();
        //baza.insertIntoTable();
        //baza.wyswietl();
        //this.konn
        //this.konn = this.konn.DriverManager.getConnection("jdbc:derby:myjdbc;create=true");
        //DatabaseMetaData dmd = this.konn.getMetaData();
        //baza.getTables();
        //baza.wyswietlAll(baza.getTables());


        while(System.in != null){
            zadanie = menu();
            //System.out.println(zadanie);
            switch (zadanie){
                case 1:
                    baza.createTable();
                    baza.insertIntoTable();
                    break;
                case 2:
                    baza.wyswietlAll(baza.getTables());
                    break;
                case 3:
                    baza.dropTable();
                    break;
                case 4:
                    System.exit(1);
                    break;
            }
        }
    }

    public static int menu(){
        int wybor;
        Scanner in =  new Scanner(System.in);
        System.out.println("\nWpisz numer zadania, ktore chcesz wykonac:");
        System.out.println("1. Załóż strukturę.");
        System.out.println("2. Wyświetl dane z bazy.");
        System.out.println("3. Usuń zawartość bazy.");
        System.out.println("4. Wyjscie.");

        wybor = in.nextInt();
        return wybor;
    }

}
