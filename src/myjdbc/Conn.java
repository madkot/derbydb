package myjdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Conn {
    private static String dbDriver = "org.apache.derby.jdbc.EmbeddedDriver";
    //private static String dbUrl = "jdbc:derby://localhost:1527/mydb;create=true";
    private static String dbUrl = "jdbc:derby:myjdbc;create=true";
    private static String dbUser = "root";
    private static String dbPwd = "root";

    Connection conn;
    public Conn(){
        try {
            //DriverManager.registerDriver(new org.apache.derby.jdbc.ClientDriver());
            //this.conn = DriverManager.getConnection(dbUrl, dbUser, dbPwd);
            this.conn = DriverManager.getConnection(dbUrl);
            if (this.conn != null) {
                System.out.println("Polaczono");
            }else{
                System.out.println("Nie polączono :(");
            }
        }
        catch(SQLException e){
            System.out.println(e + " " + e.getNextException());
        }
    }

    public void createTable(){
        try{
            conn.createStatement().execute("Create TABLE gatunki(id INT NOT NULL GENERATED ALWAYS AS IDENTITY " +
                    "CONSTRAINT gatunek_PK PRIMARY KEY,name varchar(50))");
            conn.createStatement().execute("Create TABLE artysci(id INT NOT NULL GENERATED ALWAYS AS IDENTITY " +
                    "CONSTRAINT artysta_PK PRIMARY KEY,name varchar(50))");
            conn.createStatement().execute("Create TABLE piosenki(id INT NOT NULL GENERATED ALWAYS AS IDENTITY " +
                    "CONSTRAINT piosenka_PK PRIMARY KEY,title varchar(55), artysta_id INT, gatunek_id INT  )");
            conn.createStatement().execute("ALTER TABLE piosenki ADD CONSTRAINT artysta_id_fk FOREIGN KEY (artysta_id) REFERENCES artysci(id)");
            conn.createStatement().execute("ALTER TABLE piosenki ADD CONSTRAINT gatunek_id_fk FOREIGN KEY (gatunek_id) REFERENCES gatunki(id)");

            System.out.println("Pomyslnie utworzono strukture.");
        }catch(SQLException e){
            System.out.println("CREATE: " + e);
        }
    }

    public void insertIntoTable(){
        try{
            conn.createStatement().execute("INSERT INTO gatunki (name) Values ('Rock')");
            conn.createStatement().execute("INSERT INTO gatunki (name) Values ('Metal')");
            conn.createStatement().execute("INSERT INTO gatunki (name) Values ('Jazz')");
            conn.createStatement().execute("INSERT INTO gatunki (name) Values ('Electronic')");

            conn.createStatement().execute("INSERT INTO artysci (name) Values ('Frank Sinatra')");
            conn.createStatement().execute("INSERT INTO artysci (name) Values ('Moby')");
            conn.createStatement().execute("INSERT INTO artysci (name) Values ('Korn')");
            conn.createStatement().execute("INSERT INTO artysci (name) Values ('Sound Garden')");

            conn.createStatement().execute("INSERT INTO piosenki (title, artysta_id, gatunek_id) Values ('Black Hole Sun', 4, 1)");
            conn.createStatement().execute("INSERT INTO piosenki (title, artysta_id, gatunek_id) Values ('Strangers in The Night', 1, 3)");
            conn.createStatement().execute("INSERT INTO piosenki (title, artysta_id, gatunek_id) Values ('Natural Blues', 2, 4)");
            conn.createStatement().execute("INSERT INTO piosenki (title, artysta_id, gatunek_id) Values ('Yall Want a Single', 3, 2)");

            System.out.println("Pomyslnie wstawiono przykładowe dane do bazy.");

        }catch(SQLException e){
            System.out.println("INSERT err: " + e);
        }
    }

    public void dropTable(){
        try{
            conn.createStatement().executeUpdate("Alter TABLE piosenki DROP CONSTRAINT artysta_id_fk");
            conn.createStatement().executeUpdate("Alter TABLE piosenki DROP CONSTRAINT gatunek_id_fk");

            conn.createStatement().executeUpdate("Drop TABLE gatunki ");
            conn.createStatement().executeUpdate("Drop TABLE artysci ");
            conn.createStatement().executeUpdate("Drop TABLE piosenki ");

            System.out.println("Pomyslnie usunieto dane i strukture bazy.");
        }catch(SQLException e){
            System.out.println("DROP: " + e);
        }
    }

    public void addUsers(){
        try{
            conn.createStatement().executeUpdate(
                    "CREATE TABLE Users (" +
                            "idUsers INT NOT NULL," +
                            "userName VARCHAR(100) NOT NULL ," +
                            "password VARCHAR(100) NOT NULL ," +
                            "firstName VARCHAR(100) NOT NULL ," +
                            "lastName VARCHAR(100) NOT NULL ," +
                            "description VARCHAR(255)," +
                            "PRIMARY KEY (idUsers))");
            conn.createStatement().executeUpdate("insert into USERS values(1, 'tgratkow', '123456', 'Tomasz', 'Gratkowski',null)");
            conn.createStatement().executeUpdate("insert into USERS values(2, 'akowalski', '654321', 'Artur', 'Kowalski',null)");
            conn.createStatement().executeUpdate("insert into USERS values(3, 'abukowiec', '098765', 'Arkadiusz', 'Bukowiec',null)");
        }catch(SQLException e){
            System.out.println("OUT: " + e);
        }
    }

    public void wyswietl(){
        try{
            Statement statement = this.conn.createStatement();
            ResultSet wynik = statement.executeQuery("Select * FROM piosenki");
            while(wynik.next()){
                //String colname = ;
                System.out.println( wynik.getString("title")+" "+ wynik.getString("artysta_id")+" "+ wynik.getString("gatunek_id") );
            }
        }catch(SQLException e){
            System.out.println("OUT: " + e);
        }
    }

    public void wyswietlAll(List<String> tables){
        try{
            String first = "tak";
            for (String object: tables) {
                System.out.println("\nZawartosc tabeli " + object + ":\n");
                Statement statement = this.conn.createStatement();
                ResultSet wynik = statement.executeQuery("Select * FROM "+ object);
                ResultSetMetaData rsmd = wynik.getMetaData();
                if(first.equals("tak")){
                    for(int i=1;i<=rsmd.getColumnCount();i++){
                        System.out.print( rsmd.getColumnName(i) + ", ");
                    }
                    System.out.print("\n");
                }
                while(wynik.next()){
                    for(int i=1;i<=rsmd.getColumnCount();i++){
                        System.out.print( wynik.getString(i) + ", ");
                    }
                    System.out.print("\n");
                }
            }

        }catch(SQLException e){
            System.out.println("OUT: " + e);
        }
    }

    public List<String> getTables(){
        try{
            DatabaseMetaData metaData = this.conn.getMetaData();

            ResultSet rs = metaData.getTables(null, null, "%", null);
            List<String> tables = new ArrayList<String>();
            while (rs.next()) {
                String strTableName = rs.getString("TABLE_NAME");
                String strTableSchema = rs.getString("TABLE_SCHEM");
                if(strTableSchema.equals("ROOT")){
                    tables.add(strTableName);
                }
            }
            rs.close();
            return tables;

        }catch(Exception e){
            System.out.println("DROP: " + e);
        }
        return null;
    }
}
